import {Component, Input, OnInit} from '@angular/core';
import {Video} from '../../model/Video';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {VideoService} from '../services/video/video.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-video-details',
  templateUrl: './video-details.component.html',
  styleUrls: ['./video-details.component.css']
})
export class VideoDetailsComponent implements OnInit {

  @Input('inputVideo')
  public video: Video;

  constructor(private route: ActivatedRoute,
              private  service: VideoService) {
  }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        const videoTitle = params.get('title');
        const videoPromise = this.service.findByTitle(videoTitle);
        videoPromise.catch(
          error => {
            console.error(error.errorMessage);
          }
        );
        return videoPromise;
      })
      .subscribe(video => {
        this.video = video;
      });
  }

}
