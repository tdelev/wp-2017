import {Injectable} from '@angular/core';
import {Video} from '../../../model/Video';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class VideoService {

  constructor() {
  }

  private idSequence = 4;
  /**
   * Rx subjects comparison: 
   *   http://reactivex.io/documentation/subject.html
   * Sharing data between Angular components documented at:
   *   https://angularfirebase.com/lessons/sharing-data-between-angular-components-four-methods/
   *   https://www.youtube.com/watch?v=I317BhehZKM
   *   https://www.youtube.com/watch?v=k8hMfoNIo4Y
   */
  private videoSource = new BehaviorSubject<Video[]>([{
    id: 1,
    title: 'Angular 4 Components',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/Ljd08U`,
    url: 'https://www.youtube.com/embed/giKeNoAWpOA',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }, {
    id: 2,
    title: 'Templating Basics in Angular 4',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/C9aN0U`,
    url: 'https://www.youtube.com/embed/m5k3z94rixA',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }, {
    id: 3,
    title: 'Angular 4 Property Binding',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/Z4MHTC`,
    url: 'https://www.youtube.com/embed/PKfKW5RW6k0',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }, {
    id: 4,
    title: 'Angular 4 Services Tutorial',
    description: `This video is from my Free Angular 4 Course: https://goo.gl/T5fqeB
Written tutorial: https://goo.gl/96CN2Z`,
    url: 'https://www.youtube.com/embed/daxeYiv5FHk',
    category: 'development',
    tags: ['angular', 'typescript', 'html'],
    course: 'Web Programming'
  }]);
  observedVideos = this.videoSource.asObservable();

  save(video: Video): Promise<Video> {
    // simulation of the change that the async call will make
    const videosFromServer = [];
    Object.assign(videosFromServer, this.videoSource.getValue());

    this.idSequence++;
    video.id = this.idSequence;
    videosFromServer.push(video);

    this.videoSource.next(videosFromServer);

    return Promise.resolve(video);
  }

  edit(originalVideo: Video, updatedVideo: Video): Promise<Video> {
    // simulation of the change that the async call will make
    const videosFromServer = [];
    Object.assign(videosFromServer, this.videoSource.getValue());

    const videoToChange = videosFromServer.find(i => i.id === originalVideo.id);
    Object.assign(videoToChange, updatedVideo);
    this.videoSource.next(videosFromServer);

    return Promise.resolve(updatedVideo);
  }

  findByTitle(videoTitle: string): Promise<Video> {


    const result = this.videoSource.getValue().filter(video => video.title === videoTitle);
    if (result && result.length > 0) {
      return Promise.resolve(result[0]);
    } else {
      return Promise.reject({
        errorMessage: 'No video with the given title found',
        errorCode: 404
      });
    }
  }
}
