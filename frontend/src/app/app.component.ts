import {Component, OnInit} from '@angular/core';
import {Video} from '../model/Video';
import {VideoService} from './services/video/video.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Video Aggregator';
  public videos: Video[];

  constructor(private videoService: VideoService) {

  }

  ngOnInit(): void {
    this.videoService.observedVideos.subscribe(videos => this.videos = videos);
  }

}
