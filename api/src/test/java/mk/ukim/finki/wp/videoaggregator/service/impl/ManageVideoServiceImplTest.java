package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.testing.MockCategoryRepository;
import mk.ukim.finki.wp.videoaggregator.persistence.testing.MockVideoRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ManageVideoServiceImplTest {

    ManageVideoServiceImpl manageVideoService;

    @Before
    public void setup() {
        manageVideoService = new ManageVideoServiceImpl(new MockVideoRepository()
        ,null, new MockCategoryRepository());
    }

    @Test
    public void test_create_video() {
        String title = "title";
        String url = "url";
        String description = "description";
        Long categoryId = 10L;
        Video actual  = manageVideoService.create(title, url, description, categoryId);
        assertNotNull(actual);
        assertEquals(actual.title, title);
        assertEquals(actual.url, url);
        assertEquals(actual.description, description);
        assertNotNull(actual.category);
    }



}