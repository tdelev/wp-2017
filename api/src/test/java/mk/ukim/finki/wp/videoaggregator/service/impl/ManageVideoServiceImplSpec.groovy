package mk.ukim.finki.wp.videoaggregator.service.impl

import mk.ukim.finki.wp.videoaggregator.model.exceptions.InvalidCategory
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository
import spock.lang.Specification

class ManageVideoServiceImplSpec
        extends Specification {
    VideoRepository mockVideoRepository
    CategoryRepository mockCategoryRepository

    ManageVideoServiceImpl manageVideoService

    def setup() {
        mockVideoRepository = Mock()
        mockCategoryRepository = Mock()
        manageVideoService =
                new ManageVideoServiceImpl(mockVideoRepository, null, mockCategoryRepository)
    }

    def "should fail creating video with non existing category"() {
        given:
        def categoryId = -1 // non existing
        mockCategoryRepository.findOne(categoryId) >> Optional.empty()

        when:
        manageVideoService.create("title", "url", "desc", categoryId)

        then:
        InvalidCategory ex = thrown(InvalidCategory)
        ex != null
        ex.message != null
    }

}
