package mk.ukim.finki.wp.videoaggregator.web.controllers;

import mk.ukim.finki.wp.videoaggregator.service.LastNameProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Riste Stojanov
 */
@Controller
public class HelloController {

    private LastNameProvider provider;

    @Autowired
    public HelloController(LastNameProvider provider) {
        this.provider = provider;
    }

    @RequestMapping(value = "/hello_controller", method = RequestMethod.GET)
    public String hello() {
        return "index";
    }

    @RequestMapping(value = "/hello_to", method = RequestMethod.GET)
    public ModelAndView hello(@RequestParam(defaultValue = "Riste") String name) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("name", name);
        return modelAndView;
    }

    @RequestMapping(value = "/hello_to/{name}", method = RequestMethod.GET)
    public ModelAndView helloTo(@PathVariable String name) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("name", provider.lastName(name));
        return modelAndView;
    }


    @RequestMapping(value = "/hello_invalid", method = RequestMethod.GET)
    public String helloInvalid() {
        return "invalid";
    }
}
