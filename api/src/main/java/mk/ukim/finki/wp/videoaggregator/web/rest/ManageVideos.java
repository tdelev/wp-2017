package mk.ukim.finki.wp.videoaggregator.web.rest;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.service.ManageVideoService;
import mk.ukim.finki.wp.videoaggregator.service.VideoUrlValidator;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @author Riste Stojanov
 */

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class ManageVideos {

    private final ManageVideoService service;
    private final VideoUrlValidator videoUrlValidator;

    public ManageVideos(ManageVideoService service,
                        VideoUrlValidator videoUrlValidator) {
        this.service = service;
        this.videoUrlValidator = videoUrlValidator;
    }

    @RequestMapping(value = "videos", method = RequestMethod.POST)
    public Video create(@RequestParam String title, @RequestParam String url,
                        @RequestParam String description, @RequestParam Long categoryId) {
        videoUrlValidator.validate(url);
        return service.create(title, url, description, categoryId);
    }

    @RequestMapping(value = "videos/{id}", method = RequestMethod.PATCH)
    public void updateVideo(@PathVariable Long id, @RequestParam String newTitle,
                            @RequestParam String newDescription
    ) {
        service.updateVideo(id, newTitle, newDescription);
    }

    @RequestMapping(value = "videos/{id}", method = RequestMethod.DELETE)
    public void removeVideo(@PathVariable Long id) {
        service.removeVideo(id);
    }

    @RequestMapping(value = "videos/{id}/add-tag", method = RequestMethod.PATCH)
    public void addTagToVideo(@PathVariable("id") Long videoId, @RequestParam String tag) {
        service.addTagToVideo(videoId, tag);
    }

    @RequestMapping(value = "videos/{id}/remove-tag", method = RequestMethod.PATCH)
    public void removeTagFromVideo(@PathVariable("id") Long videoId, String tag) {
        service.removeTagFromVideo(videoId, tag);
    }

    @RequestMapping(value = "videos/{id}/category", method = RequestMethod.PATCH)
    public void updateVideoCategory(@PathVariable("id") Long videoId, Long categoryId) {
        service.updateVideoCategory(videoId, categoryId);
    }
}
