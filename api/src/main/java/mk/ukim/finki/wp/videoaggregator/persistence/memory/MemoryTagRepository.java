package mk.ukim.finki.wp.videoaggregator.persistence.memory;

import mk.ukim.finki.wp.videoaggregator.model.Tag;
import mk.ukim.finki.wp.videoaggregator.persistence.TagRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Repository
public class MemoryTagRepository implements TagRepository {

    private Map<String, Tag> mapDb = new HashMap<>();

    @Override
    public Optional<Tag> findOne(String tag) {
        return Optional.ofNullable(mapDb.get(tag));
    }

    @Override
    public Tag save(Tag newTag) {
        return mapDb.put(
                newTag.name,
                new Tag(newTag.name)
        );
    }
}
