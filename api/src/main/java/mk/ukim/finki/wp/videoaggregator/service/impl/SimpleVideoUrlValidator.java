package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.exceptions.InvalidVideoUrl;
import mk.ukim.finki.wp.videoaggregator.service.VideoUrlValidator;
import org.springframework.stereotype.Component;

@Component
public class SimpleVideoUrlValidator implements VideoUrlValidator {
    private static final String HTTP_YOUTUBE_COM = "http://youtube.com";

    @Override
    public void validate(String url) {
        if (!url.startsWith(HTTP_YOUTUBE_COM)) {
            throw new InvalidVideoUrl();
        }
    }
}
