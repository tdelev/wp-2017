package mk.ukim.finki.wp.videoaggregator.model;

/**
 * @author Riste Stojanov
 */
public class Tag {

    public String name;


    public Tag(String name) {
        this.name = name;
    }

    public Tag() {

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        return name.equals(tag.name);
    }
}
