package mk.ukim.finki.wp.videoaggregator.persistence.memory;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Repository
public class MemoryCategoryRepository implements CategoryRepository {

    private static Long idSequence = 0L;
    private Map<Long, Category> mapDb = new HashMap<>();

    @Override
    public Optional<Category> findOne(Long categoryId) {
        return Optional.ofNullable(mapDb.get(categoryId));
    }

    @Override
    public Category save(Category category) {
        idSequence++;
        Category newCategory = new Category();

        newCategory.id = idSequence;
        newCategory.title = category.title;
        return mapDb.put(newCategory.id, newCategory);
    }
}
