package mk.ukim.finki.wp.videoaggregator.service;

public interface VideoUrlValidator {
    void validate(String url);
}
