package mk.ukim.finki.wp.videoaggregator.service;

import mk.ukim.finki.wp.videoaggregator.model.Video;

/**
 * @author Riste Stojanov
 */
public interface ManageVideoService {

    Video create(String title, String url, String description, Long categoryId);

    void updateVideo(Long id, String newTitle, String newDescription);

    void removeVideo(Long id);

    void addTagToVideo(Long videoId, String tag);

    void removeTagFromVideo(Long videoId, String tag);

    void updateVideoCategory(Long videoId, Long categoryId);

}
