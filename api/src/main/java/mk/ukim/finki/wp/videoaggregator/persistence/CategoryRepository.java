package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.Category;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
public interface CategoryRepository {
    Optional<Category> findOne(Long categoryId);

    Category save(Category category);
}
