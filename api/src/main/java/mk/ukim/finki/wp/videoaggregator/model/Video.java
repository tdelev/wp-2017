package mk.ukim.finki.wp.videoaggregator.model;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public class Video {
    public Long id;

    public String title;

    public String description;

    public String url;

    public Category category;

    public List<Tag> tags;
}
