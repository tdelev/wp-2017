package mk.ukim.finki.wp.videoaggregator.persistence.memory;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Repository
public class MemoryVideoRepository implements VideoRepository {

    private static Long idSequence = 0L;
    private Map<Long, Video> mapDb = new HashMap<>();

    @Override
    public Video save(Video video) {
        Video newVideo = new Video();
        idSequence++;
        newVideo.id = idSequence;
        newVideo.title = video.title;
        newVideo.description = video.description;
        newVideo.url = video.url;
        newVideo.category = video.category;
        newVideo.tags = video.tags;

        return mapDb.put(newVideo.id, newVideo);
    }

    @Override
    public Optional<Video> findOne(Long id) {
        return Optional.ofNullable(mapDb.get(id));
    }

    @Override
    public void delete(Video video) {
        mapDb.remove(video.id);
    }
}
