package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.model.Tag;
import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.DeletingVideoWithTags;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.DuplicateVideoTag;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.InvalidVideo;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.InvalidVideoTag;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import mk.ukim.finki.wp.videoaggregator.persistence.TagRepository;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import mk.ukim.finki.wp.videoaggregator.service.ManageVideoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Riste Stojanov
 */
@Service
public class ManageVideoServiceImpl implements ManageVideoService {
    static final Logger logger = LoggerFactory.getLogger(ManageVideoServiceImpl.class);

    private VideoRepository videoRepository;
    private TagRepository tagRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public ManageVideoServiceImpl(VideoRepository videoRepository, TagRepository tagRepository, CategoryRepository categoryRepository) {
        this.videoRepository = videoRepository;
        this.tagRepository = tagRepository;
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Video create(String title, String url, String description, Long categoryId) {
        Category category = categoryRepository.findOne(categoryId)
                .orElseThrow(RuntimeException::new);

        Video video = new Video();
        video.title = title;
        video.url = url;
        video.description = description;
        video.category = category;
        logger.info("Saving video [{}]", video);
        return videoRepository.save(video);
    }

    @Override
    public void updateVideo(Long id, String newTitle, String newDescription) {
        Video video = getVideoById(id);
        video.title = newTitle;
        video.description = newDescription;

        videoRepository.save(video);

    }

    @Override
    public void removeVideo(Long id) {
        Video video = getVideoById(id);

        if (video.tags != null && !video.tags.isEmpty()) {
            throw new DeletingVideoWithTags();
        }

        videoRepository.delete(video);
    }

    @Override
    public void addTagToVideo(Long videoId, String tagName) {
        Video video = getVideoById(videoId);

        if (video.tags.contains(tagName)) {
            throw new DuplicateVideoTag();
        }

        Tag tag = tagRepository.findOne(tagName)
                .orElseGet(() -> tagRepository.save(new Tag(tagName)));
        video.tags.add(tag);
        videoRepository.save(video);
    }

    @Override
    public void removeTagFromVideo(Long videoId, String tagName) {
        Video video = getVideoById(videoId);

        if (video.tags.contains(tagName)) {
            throw new InvalidVideoTag();
        }

        video.tags.remove(tagName);
        videoRepository.save(video);

    }


    @Override
    public void updateVideoCategory(Long videoId, Long categoryId) {

    }

    private Video getVideoById(Long videoId) {
        return videoRepository.findOne(videoId)
                .orElseThrow(InvalidVideo::new);
    }
}
