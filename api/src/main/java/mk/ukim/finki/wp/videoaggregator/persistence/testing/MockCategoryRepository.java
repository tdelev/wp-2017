package mk.ukim.finki.wp.videoaggregator.persistence.testing;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;

import java.util.Optional;

public class MockCategoryRepository implements CategoryRepository {
    @Override
    public Optional<Category> findOne(Long categoryId) {
        Category category = new Category();
        category.id = categoryId;
        category.title = "cat title";
        return Optional.of(category);
    }

    @Override
    public Category save(Category category) {
        return null;
    }
}
