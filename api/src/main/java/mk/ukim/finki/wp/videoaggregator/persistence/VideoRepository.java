package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.Video;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
public interface VideoRepository {

    Video save(Video video);

    Optional<Video> findOne(Long id);

    void delete(Video video);
}
