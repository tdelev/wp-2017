package mk.ukim.finki.wp.videoaggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class VideoAggregatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideoAggregatorApplication.class, args);
    }
}
