package mk.ukim.finki.wp.videoaggregator.persistence.testing;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.VideoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Random;

public class MockVideoRepository implements VideoRepository {
    static final Logger logger = LoggerFactory.getLogger(MockVideoRepository.class);

    @Override
    public Video save(Video video) {
        logger.debug("Saving video [{}]", video);
        video.id = new Random().nextLong();
        return video;
    }

    @Override
    public Optional<Video> findOne(Long id) {
        return null;
    }

    @Override
    public void delete(Video video) {

    }
}
